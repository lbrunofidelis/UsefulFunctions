
/** 
 * @param {Object} data - Objeto do tipo 'date' que representa a data de nascimento.
 * @return {number} - idade atual.
 * @description - cálculo da idade, em anos.
 */
export default function CalculaIdade(data) {
    var diaAtual = new Date().getDate();
    var mesAtual = new Date().getMonth();
    mesAtual++;    // getMonth() retorna o mês atual entre 0 e 11. Com o incremento o valor fica entre 1 e 12.
    var anoAtual = new Date().getFullYear();
    var diaNascimento = data.getDate();
    var mesNascimento = data.getMonth() + 1;
    var anoNascimento = data.getFullYear();

    var anosIdade = anoAtual - anoNascimento;

    if(mesAtual < mesNascimento)
        anosIdade--;
    
    if(mesAtual === mesNascimento)
    {
        if(diaAtual < diaNascimento)
            anosIdade--;
    }

    if(anosIdade < 0)
        anosIdade = 0;

    return anosIdade;
}

